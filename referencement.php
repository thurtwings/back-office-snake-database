<?php

//Gestion des pages
if(!isset($_GET['page'])) $_GET['page'] = 'accueil';

//Faire en sorte que le titre 
//de la page change en utilisant switch.

switch ($_GET['page']) 
{
	case 'accueil':
		$title = "Bienvenue sur le site de php";
		break;
	case 'admin':
		$title = 'Administration';
		break;
	case 'gestion':
		$title = 'Gestion';
		break;
	case 'modifobj':
		$title = 'Modifier';
		break;
	case 'insertobj':
		$title = 'Ajouter';
		break;
	case 'killSnake':
		$title = 'Boucherie';
		break;
	default :
		$title = "Un site PHP";
		break;
}

?>