<?php
    session_start();
    
    include("classes/class_objet.php");

if (isset($_POST['log'])) 
{
    //connection a la bdd en PDO
    $bdd = new PDO('mysql:host=localhost;dbname=backoffice_vivarium', 'root', '');
    //création requête SQL
    $req = "SELECT * FROM `users` WHERE user_pseudo = '" . $_POST['log'] . "'AND user_password = '" . $_POST['pwd']."'";
    //execution requête
    $result = $bdd->query($req);
    //traitement du résultat ( le transforme en tableau)
    $tlbresult = $result ->fetchAll();
    //var_dump($tlbresult);
    //test si un enregistrement est présent en BDD, si oui on se connecte sinon, 
    //on ne se connecte pas et on informe l'utilisateur

    if (count($tlbresult) > 0)
    {
        $_SESSION['user_name'] = $tlbresult[0]['user_pseudo'];
        $_SESSION['user_id'] = $tlbresult[0]['user_id'];
    }
    else
    {
        echo "Mot de passe incorrect";
    }
}


if (isset($_POST['deco1']) || isset( $_GET['deco1']))
{
    unset($_SESSION);
    session_destroy();
}



//gestion des pages
include("referencement.php");

?>
<!DOCTYPE html>
<html>
	<head lang="fr">
		<meta charset="utf-8" />
		<link
  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.1.0/mdb.min.css"
  rel="stylesheet"
/>
		<title><?php echo $title; ?></title>
	</head>
	<body>
		
		<?php if(isset($_SESSION['user_name'])) echo "Bonjour : ".$_SESSION['user_name']; ?>

		<?php include('menu.php'); ?>

		<div id="main">
			<?php 
			if(file_exists('page/'.$_GET['page'].'.php')) {
				include('page/'.$_GET['page'].'.php');
			} else {
				//include('page/accueil.php');
				echo "Oups, la page recherché n'est pas disponible.";
			}
			?>
		</div>

		<footer>
			<div class="container-fluid">
				<div class="row">
					<h6 class="text-center ">Copyright de moi</h6>

				</div>

			</div>
		</footer>
	</body>
</html>