<?php
class Objet 
{
	
	private $tbl = "snakes";
	private $id = "";
	private $sql = "";

	public function __construct($id) 
	{
		$this->id = $id;
		$this->sql = new PDO('mysql:host=localhost;dbname=backoffice_vivarium','root','');
	}


	public function selectAll() 
	{
		$req = "SELECT * FROM `".$this->tbl."`";
		$result = $this->sql->query($req);
		$tblresult = $result->fetchAll();

		return $tblresult;
	}

	public function get($column) 
	{
		$req = "SELECT `".$column."` FROM `".$this->tbl."` WHERE `id` = '".$this->id."'";
		$result = $this->sql->query($req);
		$tblresult = $result->fetchAll();

		return $tblresult[0][0];
	}

	public function set($column, $value) 
	{
		$req = "UPDATE `".$this->tbl."` SET `".$column."` = '".$value."' WHERE `id` = '".$this->id."'";
		$this->sql->query($req);
	}

	public function addNew($name, $weight, $lifespan, $dateOfBirth, $specie, $gender)
    {
        $req = "INSERT INTO ".$this->tbl." (`snake_id`, `snake_name`, `snake_weight`, `snake_lifespan`, `snake_H_DoB`, `snake_specie`, `snake_gender`, `snake_dead`) VALUES (NULL, '".$name."','".$weight."', '".$lifespan."', '".$dateOfBirth."', '".$specie."', '".$gender."', '0')";
        
		$this->sql->query($req);
    }

	
	public function KillSnake($id)
    {
        $req = "UPDATE `".$this->tbl."` SET `snake_dead` = '1' WHERE `".$this->tbl."`.`snake_id` =".$id; 	
		$req = "SELECT COUNT(*) FROM snakes WHERE snakes.snake_gender = " . $id . ";";
		$this->sql->query($req);
    }

	

}
?>